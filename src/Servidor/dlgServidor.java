package Servidor;

import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class dlgServidor extends javax.swing.JDialog {

    private JPanel figuraPanel;
    private static JDialog figuraDialog; // JDialog para mostrar la figura
    private String inputLine;
    private DataInputStream entrada;
    private final int PUERTO = 5432;
    private ServerSocket serverSocket = null;
    private boolean servidorActivo = false;
    Mensajes hiloMensaje = new Mensajes();

    public dlgServidor(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setSize(300, 390);
        setTitle(":: SERVIDOR ::");
    }
    
    public void paint(Graphics g, String inpLi) {
        super.paintComponents(g);
        int x = getWidth() / 2;
        int y = getHeight() / 2;
        int radio = 50;
        switch (inpLi) {
            case "11":
                g.setColor(Color.BLUE);
                g.fillOval(x - radio, y - radio, radio * 2, radio * 2);
                //g.fillOval(210, 70, 80, 80);
                break;
            case "12":
                g.setColor(Color.GREEN);
                g.fillOval(x - radio, y - radio, radio * 2, radio * 2);
                //g.fillOval(210, 70, 80, 80);
                break;
            case "13":
                g.setColor(Color.RED);
                g.fillOval(x - radio, y - radio, radio * 2, radio * 2);
                //g.fillOval(210, 70, 80, 80);
                break;
            case "21":
                g.setColor(Color.BLUE);
                g.fillRect(x - radio, y - radio, radio * 2, radio);
                //g.fillRect(210, 80, 80, 60);
                break;
            case "22":
                g.setColor(Color.GREEN);
                g.fillRect(x - radio, y - radio, radio * 2, radio);
                //g.fillRect(210, 80, 80, 60);
                break;
            case "23":
                g.setColor(Color.RED);
                g.fillRect(x - radio, y - radio, radio * 2, radio);
                //g.fillRect(210, 80, 80, 60);
                break;
            default:
                System.out.println("Figura no encontrada");
                break;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jButton1.setText("Iniciar Servidor");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(70, 250, 120, 23);

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel2.setText("Objeto Recibido");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(60, 30, 150, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
            hiloMensaje = new Mensajes();
            hiloMensaje.start();


        /*Mensajes conecc = new Mensajes();
        conecc.start();*/
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgServidor dialog = new dlgServidor(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables

    /*class Mensajes extends Thread {

        private boolean continuarServidor = true;

        public void detenerServidor() {
            continuarServidor = false;
        }

        @Override
        public void run() {
                try {
                    serverSocket = new ServerSocket(PUERTO);
                    System.out.println("Servidor iniciado en el puerto: " + PUERTO);
                } catch (IOException e) {
                    System.err.println("No se puede escuchar por el puerto:" + PUERTO);
                    System.exit(1);
                }

                Socket clientSocket = null;
                try {
                    clientSocket = serverSocket.accept();
                    System.out.println("Cliente conectado desde: " + clientSocket.getInetAddress());
                    entrada = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
                } catch (IOException e) {
                    System.err.println("Conexión fallida...");
                    System.exit(1);
                }

                while (continuarServidor) {
                    try {
                        inputLine = entrada.readLine();
                        if (inputLine != null) {
                            System.out.println("Figura recibida del cliente: " + numeroInt);
                            figuraPanel.repaint(); // Repintar el JPanel para mostrar la figura actualizada
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(dlgServidor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                try {
                    clientSocket.close();
                    entrada.close();
                } catch (IOException e) {
                    System.out.println("No se pudo cerrar el socket o Stream");
                }
            }
        }
    }*/
    
    
    
    class Mensajes extends Thread {

        public void run() {
            try {
                serverSocket = new ServerSocket(PUERTO);
            } catch (IOException e) {
                System.err.println("No se puede escuchar por el puerto:" + PUERTO);
                System.exit(1);
            }

            Socket clientSocket = null;  // inicia el puerto cliente
            try {
                clientSocket = serverSocket.accept();// se conecta los Socket
                entrada = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
            } catch (IOException e) {
                System.err.println("Conexión fallida...");
                System.exit(1);
            }
            while (true) {
                try {
                    inputLine = entrada.readLine();
                } catch (IOException e) {
                    System.out.println("El Mensaje no pudo ser Entregado");
                }
                if (inputLine.equals("Exit")) {
                    //txtMensajes.append("Adios ya me Voy");
                    break;
                } else if (!inputLine.equals(null)) {
                    Graphics g = getGraphics();
                    if (g != null) {
                        paint(g, inputLine);
                    }
                }
               // txtMensajes.append(inputLine + "\n"); //Poner en JtextArea
            }  // cerrar while
            try {
                clientSocket.close();
                entrada.close();

            } catch (IOException e) {
                System.out.println("No se puedo cerrar el socket o Stream");
            }

        }  //cerrar el metodo run 
    } // cerrar la clase Hilo     
} // cerra  clase
